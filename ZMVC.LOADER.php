<?php

/*
 *  ZeroMVC @ NICHER CL
 *  PROJECT LOADER FILE
 *  PROJECT NAME
 *  AUTHOR
 */


// LOAD COMPOSER DEPENDENCIES
require_once __DIR__ . '/vendor/autoload.php';

// MAIL
// $mail = new \PHPMailer\PHPMailer\PHPMailer();
//
// LOAD ENVIROMENT
// require_once __DIR__ . '/enviroment/env_prod.inc.php';
require_once __DIR__ . '/enviroment/env_dev.inc.php';

// TEMPLATE
$twig_loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/templates/');
$template = new \Twig\Environment($twig_loader, array());

// $template->addGlobal("session", $_SESSION);
// $template->addGlobal("THIS_URL", $_SERVER['REQUEST_URI']);
$template->addGlobal("HOME_URL", HOME_URL);

// LOAD CONTROLLERS
foreach (glob(__DIR__ . "/controller/*.php") as $file) {
    require $file;
}

// LOAD ROUTES
use Phroute\Phroute\RouteCollector;

$router = new RouteCollector();
require __DIR__ . '/backend/router.inc.php';
$dispatcher = new Phroute\Phroute\Dispatcher($router->getData());

// ROUTE + RENDER

if (preg_match('/\.(?:woff|woff2|ttf|map|js|css|png)[?]*[a-z-=0-9.@]*$/', $_SERVER["REQUEST_URI"])) {
    return false;
} else {

    try {
        echo $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
    } catch (Phroute\Phroute\Exception\HttpRouteNotFoundException $e) {
        echo '404';
        // echo $template->render('error404.twig');
    } catch (Phroute\Phroute\Exception\HttpMethodNotAllowedException $e) {
        echo '404';
        // echo $template->render('error404.twig');
    }
}