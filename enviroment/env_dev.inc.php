<?php

/*
 *
 * ZeroMVC @ NICHER CL
 * DEVELOPMENT ENVIROMENT FILE
 *
 */

// URL SETTINGS
define("BASE_FQDN", '//localhost:8000');
define("BASE_URL", '');
define("HOME_URL", BASE_FQDN . BASE_URL);

// FILE SETTINGS
define("BASE_DIR", getcwd());
define("UPLOAD_DIR", "/uploads");

// PROFILE IMG SETTINGS
define("UPLOAD_PROFILE_IMG_DIR", UPLOAD_DIR . "/intranet/profile_pictures");
define("MAX_PROFILE_IMG_UPLOAD_SIZE", 2000 * 1024);

// DATABASE
define("DB_HOST", "sat17.nicher.cl");
define("DB_NAME", "nicher_cl");
define("DB_USER", "postgres");
define("DB_SECRET", "m2opwQOsqxcglFWy92gG");

// MANEJO DE ERRORES
//  0 |-1 => NINGUNO | TODOS
error_reporting(-1);

// SET LOCALE CHILE
setlocale(LC_ALL, "es_CL.UTF-8");
date_default_timezone_set('America/Santiago');

// SMTP
//$mail->isSMTP();
//$mail->Host = 'smtp.mailgun.org';
//$mail->SMTPAuth = true;
//$mail->Username = 'contacto-web@mg.nicher.cl';
//$mail->Password = 'contacto2018';
//$mail->SMTPSecure = 'tls';
//$mail->Port = 587;
// SESSION
session_start();
