<?php

/*
 *
 * ZeroMVC @ NICHER CL
 * DATABASE CONFIG @ DOCTRINE DBAL
 *
 */

$doctrineConfig = new \Doctrine\DBAL\Configuration();

$databaseParams = array(
    'dbname' => DB_NAME,
    'user' => DB_USER,
    'password' => DB_SECRET,
    'host' => DB_HOST,
    'driver' => 'pdo_pgsql',
);
