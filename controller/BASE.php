<?php

/*
 *
 * ZeroMVC @ NICHER CL
 * EXAMPLE CONTROLLER FILE
 *
 */

namespace Controller;

class BASE {

    function pageStart() {
        global $template;
        return $template->render('BASE.twig');
    }

}
